package com.serebit.strife.events

import com.serebit.strife.*
import com.serebit.strife.data.Presence
import com.serebit.strife.data.VoiceState
import com.serebit.strife.internal.EventResult
import kotlin.reflect.typeOf

/**
 * DSL marker for extension functions on the class [Event].
 */
@DslMarker
annotation class EventDsl

/**
 * Creates a terminal event listener for events with type [T]. The code inside the [task] block will be executed every time the
 * bot receives an event with type [T].
 */
@EventDsl
@UseExperimental(ExperimentalStdlibApi::class)
inline fun <reified T : Event> Event.onEventLimited(
    successfulRunLimit: Int = 1,
    noinline task: suspend T.() -> EventResult
): Unit = context.onTerminableEvent(typeOf<T>(), successfulRunLimit, task)

/** Creates a terminal event listener that will execute when the bot receives any event type. */
@EventDsl
fun Event.onAnyEventLimited(successfulRunLimit: Int = 1, task: suspend Event.() -> EventResult) = context.onAnyEventLimited(successfulRunLimit, task)

// ==> Status Events //

/** Convenience method to create a terminal event listener that will execute when the bot starts a session. */
@EventDsl
fun Event.onReadyLimited(successfulRunLimit: Int = 1, task: suspend ReadyEvent.() -> EventResult) = context.onReadyLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when the bot resumes a session. */
@EventDsl
fun Event.onResumeLimited(successfulRunLimit: Int = 1, task: suspend ResumeEvent.() -> EventResult) = context.onResumeLimited(successfulRunLimit, task)

// ==> Message & Reaction Events //

/** Convenience method to create a terminal event listener that will execute when a message is created. */
@EventDsl
fun Event.onMessageCreateLimited(successfulRunLimit: Int = 1, task: suspend MessageCreateEvent.() -> EventResult) = context.onMessageCreateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a message's content is edited. */
@EventDsl
fun Event.onMessageEditLimited(successfulRunLimit: Int = 1, task: suspend MessageEditEvent.() -> EventResult) = context.onMessageEditLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a message is deleted. */
@EventDsl
fun Event.onMessageDeleteLimited(successfulRunLimit: Int = 1, task: suspend MessageDeleteEvent.() -> EventResult) = context.onMessageDeleteLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when multiple messages are deleted at once. */
@EventDsl
fun Event.onMessageDeleteBulkLimited(successfulRunLimit: Int = 1, task: suspend MessageBulkDeleteEvent.() -> EventResult) = context.onMessageDeleteBulkLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a message reaction is added. */
@EventDsl
fun Event.onReactionAddLimited(successfulRunLimit: Int = 1, task: suspend MessageReactionAddEvent.() -> EventResult) = context.onReactionAddLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a message reaction is removed. */
@EventDsl
fun Event.onReactionRemoveLimited(successfulRunLimit: Int = 1, task: suspend MessageReactionRemoveEvent.() -> EventResult) = context.onReactionRemoveLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when all reactions are cleared from a message . */
@EventDsl
fun Event.onReactionClearLimited(successfulRunLimit: Int = 1, task: suspend MessageReactionRemoveAllEvent.() -> EventResult) = context.onReactionClearLimited(successfulRunLimit, task)

// ==> Channel Events //

/** Convenience method to create a terminal event listener that will execute when a channel is created. */
@EventDsl
fun Event.onChannelCreateLimited(successfulRunLimit: Int = 1, task: suspend ChannelCreateEvent.() -> EventResult) = context.onChannelCreateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a channel is updated. */
@EventDsl
fun Event.onChannelUpdateLimited(successfulRunLimit: Int = 1, task: suspend ChannelUpdateEvent.() -> EventResult) = context.onChannelUpdateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a channel is deleted. */
@EventDsl
fun Event.onChannelDeleteLimited(successfulRunLimit: Int = 1, task: suspend ChannelDeleteEvent.() -> EventResult) = context.onChannelDeleteLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a channel's pinned messages are updated. */
@EventDsl
fun Event.onChannelPinsUpdateLimited(successfulRunLimit: Int = 1, task: suspend ChannelPinsUpdateEvent.() -> EventResult) = context.onChannelPinsUpdateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a user begins typing in a channel. */
@EventDsl
fun Event.onTypingStartLimited(successfulRunLimit: Int = 1, task: suspend TypingStartEvent.() -> EventResult) = context.onTypingStartLimited(successfulRunLimit, task)

// ==> Guild events //

/** Convenience method to create a terminal event listener that will execute when a Guild is created or joined. */
@EventDsl
fun Event.onGuildCreateLimited(successfulRunLimit: Int = 1, task: suspend GuildCreateEvent.() -> EventResult) = context.onGuildCreateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild is updated. */
@EventDsl
fun Event.onGuildUpdateLimited(successfulRunLimit: Int = 1, task: suspend GuildUpdateEvent.() -> EventResult) = context.onGuildUpdateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild becomes unavailable. */
@EventDsl
fun Event.onGuildDeleteLimited(successfulRunLimit: Int = 1, task: suspend GuildDeleteEvent.() -> EventResult) = context.onGuildDeleteLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild Ban is added. */
@EventDsl
fun Event.onGuildBanAddLimited(successfulRunLimit: Int = 1, task: suspend GuildBanAddEvent.() -> EventResult) = context.onGuildBanAddLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild Ban is removed. */
@EventDsl
fun Event.onGuildBanRemoveLimited(successfulRunLimit: Int = 1, task: suspend GuildBanRemoveEvent.() -> EventResult) = context.onGuildBanRemoveLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a User joins a Guild. */
@EventDsl
fun Event.onGuildMemberJoinLimited(successfulRunLimit: Int = 1, task: suspend GuildMemberJoinEvent.() -> EventResult) = context.onGuildMemberJoinLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild Member is updated. */
@EventDsl
fun Event.onGuildMemberUpdateLimited(successfulRunLimit: Int = 1, task: suspend GuildMemberUpdateEvent.() -> EventResult) = context.onGuildMemberUpdateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a User leaves a Guild . */
@EventDsl
fun Event.onGuildMemberLeaveLimited(successfulRunLimit: Int = 1, task: suspend GuildMemberLeaveEvent.() -> EventResult) = context.onGuildMemberLeaveLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild Role is created. */
@EventDsl
fun Event.onGuildRoleCreateLimited(successfulRunLimit: Int = 1, task: suspend GuildRoleCreateEvent.() -> EventResult) = context.onGuildRoleCreateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild Role is updated. */
@EventDsl
fun Event.onGuildRoleUpdateLimited(successfulRunLimit: Int = 1, task: suspend GuildRoleUpdateEvent.() -> EventResult) = context.onGuildRoleUpdateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild Role is deleted. */
@EventDsl
fun Event.onGuildRoleDeleteLimited(successfulRunLimit: Int = 1, task: suspend GuildRoleDeleteEvent.() -> EventResult) = context.onGuildRoleDeleteLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild's Emoji are updated. */
@EventDsl
fun Event.onGuildEmojiUpdateLimited(successfulRunLimit: Int = 1, task: suspend GuildEmojisUpdateEvent.() -> EventResult) = context.onGuildEmojiUpdateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild's integrations have been update. */
@EventDsl
fun Event.onGuildIntegrationsUpdateLimited(successfulRunLimit: Int = 1, task: suspend GuildIntegrationsUpdateEvent.() -> EventResult) = context.onGuildIntegrationsUpdateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a User's [Presence] is updated. */
@EventDsl
fun Event.onPresenceUpdateLimited(successfulRunLimit: Int = 1, task: suspend PresenceUpdateEvent.() -> EventResult) = context.onPresenceUpdateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a User's [VoiceState] is updated. */
@EventDsl
fun Event.onVoiceStateUpdateLimited(successfulRunLimit: Int = 1, task: suspend VoiceStateUpdateEvent.() -> EventResult) = context.onVoiceStateUpdateLimited(successfulRunLimit, task)

/**
 * Convenience method to create a terminal event listener that will execute when a Guild's VoiceServer is updated.
 * [see](https://discordapp.com/developers/docs/topics/gateway#voice-server-update)
 */
@EventDsl
fun Event.onVoiceServerUpdateLimited(successfulRunLimit: Int = 1, task: suspend VoiceServerUpdateEvent.() -> EventResult) = context.onVoiceServerUpdateLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild's webhook is updated. */
@EventDsl
fun Event.onWebhookUpdateLimited(successfulRunLimit: Int = 1, task: suspend WebhookUpdateEvent.() -> EventResult) = context.onWebhookUpdateLimited(successfulRunLimit, task)
