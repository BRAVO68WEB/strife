package com.serebit.strife

import com.serebit.strife.data.Presence
import com.serebit.strife.data.VoiceState
import com.serebit.strife.events.*
import com.serebit.strife.internal.EventResult
import kotlin.reflect.typeOf

/**
 * DSL marker for extension functions on the class [BotClient].
 */
@DslMarker
annotation class BotClientDsl

/**
 * Creates a terminal event listener for events with type [T]. The code inside the [task] block will be executed every time the
 * bot receives an event with type [T].
 */
@BotClientDsl
@UseExperimental(ExperimentalStdlibApi::class)
inline fun <reified T : Event> BotClient.onEventLimited(
    successfulRunLimit: Int = 1,
    noinline task: suspend T.() -> EventResult
): Unit = onTerminableEvent(typeOf<T>(), successfulRunLimit, task)

/** Creates a terminal event listener that will execute when the bot receives any event type. */
@BotClientDsl
fun BotClient.onAnyEventLimited(successfulRunLimit: Int = 1, task: suspend Event.() -> EventResult) = onEventLimited(successfulRunLimit, task)

// ==> Status Events //

/** Convenience method to create a terminal event listener that will execute when the bot starts a session. */
@BotClientDsl
fun BotClient.onReadyLimited(successfulRunLimit: Int = 1, task: suspend ReadyEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when the bot resumes a session. */
@BotClientDsl
fun BotClient.onResumeLimited(successfulRunLimit: Int = 1, task: suspend ResumeEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

// ==> Message & Reaction Events //

/** Convenience method to create a terminal event listener that will execute when a message is created. */
@BotClientDsl
fun BotClient.onMessageCreateLimited(successfulRunLimit: Int = 1, task: suspend MessageCreateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a message's content is edited. */
@BotClientDsl
fun BotClient.onMessageEditLimited(successfulRunLimit: Int = 1, task: suspend MessageEditEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a message is deleted. */
@BotClientDsl
fun BotClient.onMessageDeleteLimited(successfulRunLimit: Int = 1, task: suspend MessageDeleteEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when multiple messages are deleted at once. */
@BotClientDsl
fun BotClient.onMessageDeleteBulkLimited(successfulRunLimit: Int = 1, task: suspend MessageBulkDeleteEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a message reaction is added. */
@BotClientDsl
fun BotClient.onReactionAddLimited(successfulRunLimit: Int = 1, task: suspend MessageReactionAddEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a message reaction is removed. */
@BotClientDsl
fun BotClient.onReactionRemoveLimited(successfulRunLimit: Int = 1, task: suspend MessageReactionRemoveEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when all reactions are cleared from a message . */
@BotClientDsl
fun BotClient.onReactionClearLimited(successfulRunLimit: Int = 1, task: suspend MessageReactionRemoveAllEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

// ==> Channel Events //

/** Convenience method to create a terminal event listener that will execute when a channel is created. */
@BotClientDsl
fun BotClient.onChannelCreateLimited(successfulRunLimit: Int = 1, task: suspend ChannelCreateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a channel is updated. */
@BotClientDsl
fun BotClient.onChannelUpdateLimited(successfulRunLimit: Int = 1, task: suspend ChannelUpdateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a channel is deleted. */
@BotClientDsl
fun BotClient.onChannelDeleteLimited(successfulRunLimit: Int = 1, task: suspend ChannelDeleteEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a channel's pinned messages are updated. */
@BotClientDsl
fun BotClient.onChannelPinsUpdateLimited(successfulRunLimit: Int = 1, task: suspend ChannelPinsUpdateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a user begins typing in a channel. */
@BotClientDsl
fun BotClient.onTypingStartLimited(successfulRunLimit: Int = 1, task: suspend TypingStartEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

// ==> Guild events //

/** Convenience method to create a terminal event listener that will execute when a Guild is created or joined. */
@BotClientDsl
fun BotClient.onGuildCreateLimited(successfulRunLimit: Int = 1, task: suspend GuildCreateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild is updated. */
@BotClientDsl
fun BotClient.onGuildUpdateLimited(successfulRunLimit: Int = 1, task: suspend GuildUpdateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild becomes unavailable. */
@BotClientDsl
fun BotClient.onGuildDeleteLimited(successfulRunLimit: Int = 1, task: suspend GuildDeleteEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild Ban is added. */
@BotClientDsl
fun BotClient.onGuildBanAddLimited(successfulRunLimit: Int = 1, task: suspend GuildBanAddEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild Ban is removed. */
@BotClientDsl
fun BotClient.onGuildBanRemoveLimited(successfulRunLimit: Int = 1, task: suspend GuildBanRemoveEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a User joins a Guild. */
@BotClientDsl
fun BotClient.onGuildMemberJoinLimited(successfulRunLimit: Int = 1, task: suspend GuildMemberJoinEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild Member is updated. */
@BotClientDsl
fun BotClient.onGuildMemberUpdateLimited(successfulRunLimit: Int = 1, task: suspend GuildMemberUpdateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a User leaves a Guild . */
@BotClientDsl
fun BotClient.onGuildMemberLeaveLimited(successfulRunLimit: Int = 1, task: suspend GuildMemberLeaveEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild Role is created. */
@BotClientDsl
fun BotClient.onGuildRoleCreateLimited(successfulRunLimit: Int = 1, task: suspend GuildRoleCreateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild Role is updated. */
@BotClientDsl
fun BotClient.onGuildRoleUpdateLimited(successfulRunLimit: Int = 1, task: suspend GuildRoleUpdateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild Role is deleted. */
@BotClientDsl
fun BotClient.onGuildRoleDeleteLimited(successfulRunLimit: Int = 1, task: suspend GuildRoleDeleteEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild's Emoji are updated. */
@BotClientDsl
fun BotClient.onGuildEmojiUpdateLimited(successfulRunLimit: Int = 1, task: suspend GuildEmojisUpdateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild's integrations have been update. */
@BotClientDsl
fun BotClient.onGuildIntegrationsUpdateLimited(successfulRunLimit: Int = 1, task: suspend GuildIntegrationsUpdateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a User's [Presence] is updated. */
@BotClientDsl
fun BotClient.onPresenceUpdateLimited(successfulRunLimit: Int = 1, task: suspend PresenceUpdateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a User's [VoiceState] is updated. */
@BotClientDsl
fun BotClient.onVoiceStateUpdateLimited(successfulRunLimit: Int = 1, task: suspend VoiceStateUpdateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/**
 * Convenience method to create a terminal event listener that will execute when a Guild's VoiceServer is updated.
 * [see](https://discordapp.com/developers/docs/topics/gateway#voice-server-update)
 */
@BotClientDsl
fun BotClient.onVoiceServerUpdateLimited(successfulRunLimit: Int = 1, task: suspend VoiceServerUpdateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)

/** Convenience method to create a terminal event listener that will execute when a Guild's webhook is updated. */
@BotClientDsl
fun BotClient.onWebhookUpdateLimited(successfulRunLimit: Int = 1, task: suspend WebhookUpdateEvent.() -> EventResult) = onEventLimited(successfulRunLimit, task)
